echo "Installing unzip..."
apt-get install unzip
apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
apt-get install -y libopenmpi-dev
echo "Installing dependencies..."
pip install pipenv
rm /opt/conda/compiler_compat/ld
pipenv install --system
mkdir -p datasets
chmod +x ${PWD}/scripts/download_plant_pathology_2020.sh
sh ${PWD}/scripts/download_plant_pathology_2020.sh
python ${PWD}/scripts/generate_plant_pathology_2020_dataset.py

# pip install blobfile
# pip install mpi4py