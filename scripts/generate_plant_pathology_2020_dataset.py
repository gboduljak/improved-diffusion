#!/usr/bin/env python3

import os
import sys
from os import path
from pathlib import Path
from typing import List, Union

import numpy as np
import pandas as pd
import torch
from PIL import Image
from torch.utils.data import Dataset, DataLoader, random_split
from torchvision.transforms import ToTensor, ToPILImage


class PlantPathology2020Raw(Dataset):
  def __init__(self,
               root: str,
               transform=None,
               target_transform=None,
               selected_classes: Union[List[str], None] = None
               ):
    self.__img_path = Path(root, 'images')

    # test images were used for competition, they do not have labels
    all_classes = ['healthy', 'multiple_diseases', 'rust', 'scab']

    if not selected_classes:
      selected_classes = all_classes
    else:
      for c in selected_classes:
        assert c in all_classes, f'class {c} was not found'

    self.classes = selected_classes
    self.transform = transform
    self.target_transform = target_transform

    train_frame = pd.read_csv(Path(root, 'train.csv'))

    relevant_imgs = train_frame[selected_classes].to_numpy().sum(axis=1) == 1
    train_subframe = train_frame.loc[relevant_imgs]
    train_subframe = train_subframe[['image_id'] + selected_classes]

    self.class_to_idx = dict({
        (c, i) for (i, c) in enumerate(self.classes)
    })
    self.imgs = pd.DataFrame({
        'image_id': train_subframe['image_id'],
        'class':  np.argmax(train_subframe[selected_classes].to_numpy(), axis=1)
    }).to_numpy().tolist()

  def __len__(self):
    return len(self.imgs)

  def __getitem__(self, index):
    [img, img_class] = self.imgs[index]

    img = Image.open(Path(self.__img_path, f'{img}.jpg')).convert('RGB')
    if self.transform:
      img = self.transform(img)
    if self.target_transform:
      img_class = self.target_transform(img_class)
    return img, img_class


DATASET_SPLIT_SEED = 1726441

train_ratio = 0.8
val_ratio = 0.1
test_ratio = 0.1
img_size = 256

full_plant_pathology_2020 = PlantPathology2020Raw(
    root='datasets/plant-pathology-2020-fgvc7-raw',
    transform=ToTensor()
)
full_plant_pathology_loader = DataLoader(dataset=full_plant_pathology_2020)

idx_to_class = {
    v: k for (k, v) in full_plant_pathology_2020.class_to_idx.items()}
idx_to_class

source_domain = 'healthy'
target_domain = 'rust'
domain_shorthand = {
    source_domain: 'A',
    target_domain: 'B'
}
dataset_name = f'plant-pathology-2020-fgvc7_{source_domain}_{target_domain}'

generated_dataset_path = f'datasets/{dataset_name}'

if not os.path.exists(generated_dataset_path):
  os.makedirs(generated_dataset_path)


for (idx, example) in enumerate(full_plant_pathology_loader):
  (img, label) = example
  domain = idx_to_class[label.item()]
  if domain in [source_domain, target_domain]:
    print(f'{domain}_{idx}.jpg')
    ToPILImage()(img.squeeze()).resize((img_size, img_size)).save(
        Path(generated_dataset_path, f'{domain}_{idx}.jpg'), 'JPEG')
